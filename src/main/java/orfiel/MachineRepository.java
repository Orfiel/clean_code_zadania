package orfiel;

import java.util.List;

public interface MachineRepository {

    void add(Machine machine);

    boolean remove(Machine machine);

    List<Machine> getAll();

    List<Machine> findBy(MachineType machineType);

    List<Machine> findByName(String machineName);


}
