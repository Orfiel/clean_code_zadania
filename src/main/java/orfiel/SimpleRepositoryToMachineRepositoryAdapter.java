package orfiel;

import org.omg.CORBA.MARSHAL;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SimpleRepositoryToMachineRepositoryAdapter implements MachineRepository{

    private SimpleRepository simpleRepository;

    public SimpleRepositoryToMachineRepositoryAdapter(SimpleRepository simpleRepository) {
        this.simpleRepository = simpleRepository;
    }

    @Override
    public void add(Machine machine) {
        simpleRepository.add(machine);

    }

    @Override
    public boolean remove(Machine machine) {
        return this.simpleRepository.remove(machine);
    }

    @Override
    public List<Machine> getAll() {
        ArrayList<Machine> list = new ArrayList();
        for (MachineType item : MachineType.values()) {
            list.addAll(simpleRepository.find(item));
        }
        return list;
    }

    @Override
    public List<Machine> findBy(MachineType machineType) {
        return this.simpleRepository.find(machineType);
    }

    @Override
    public List<Machine> findByName(String machineName) {

        return getAll().stream()
                .filter((machine -> machine.getName().equals(machineName)))
                .collect(Collectors.toList());
    }
}
