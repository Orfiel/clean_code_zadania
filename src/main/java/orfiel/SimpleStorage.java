package orfiel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SimpleStorage implements Storage{

    private List<Machine> dataStorage = new ArrayList<Machine>();

    public void addMachine(Machine machine) {
        if (machine == null){
            throw new RuntimeException("Worning ");
        }
        dataStorage.add(machine);
    }

    public boolean removeMachine(Machine machine) {
        return dataStorage.remove(machine);
    }

    public List<Machine> getMachines() {
        return getMachines().stream().collect(Collectors.toList());
    }
}
