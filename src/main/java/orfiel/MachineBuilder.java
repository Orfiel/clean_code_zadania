package orfiel;

public class MachineBuilder {

    String name;
    MachineType type;
    public MachineBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public MachineBuilder witchMachineType(MachineType type) {
        this.type = type;
        return this;
    }
    public Machine build(){
        Machine machine = new Machine();
        machine.setName(this.name);
        machine.setType(this.type);
        return machine;
    }

}
