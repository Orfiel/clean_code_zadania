package orfiel;

import java.util.List;

public interface Storage {

    void addMachine(Machine machine);

    boolean removeMachine(Machine machine);

    List<Machine> getMachines();


}
