package orfiel;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class SimpleRepository implements Repository{

    private SimpleStorage storage;

    public SimpleRepository(SimpleStorage storage) {
        this.storage = storage;
    }

    @Override
    public void add(Machine machine) {
        storage.addMachine(machine);

    }

    @Override
    public boolean remove(Machine machine) {
        return storage.removeMachine(machine);
    }

    @Override
    public List<Machine> find(MachineType machineType) {
        return storage.getMachines().stream()
                .filter(machine -> machine.getType().equals(machineType))
                .collect(Collectors.toList());
    }

}
