package orfiel;

import java.util.List;

public interface Repository{

    void add(Machine machine);

    boolean remove(Machine machine);

    List<Machine> find(MachineType machineType);


}
